---
title: Edge Router Automation
summary: A simple set of Ansible Scripts to configure the Ubiquiti Edge Router.
tags:
- Automation
- Edge Router
- Networking
- Ubiquiti
date: "2019-01-23T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Ubiquiti Logo
  focal_point: Smart

links:
- icon: twitter
  icon_pack: fab
  name: Follow
  url: https://twitter.com/ryan-n-gay
url_code: "https://github.com/ryan-n-gay/edgerouter-automation.git"
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

A former co-worker of mine introduced me to automation started the process of developing this script. After my initial introduction I began to find other features that I could script out and implement with this script. This script allows for one touch configuration after going through the initial configuration issue. There is still more to come.

At this point the script can be used to implement the following tasks:

* AD Block
* Dynamic DNS
* IPsec Server Creation
* Let's Encrypt Deployment
* WireGuard for AWS