+++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Accomplish&shy;ments"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  organization = "Juniper"
  organization_url = "https://www.juniper.com"
  title = "JNCIA-Junos"
  url = ""
  certificate_url = "https://www.youracclaim.com/badges/860d8623-9a2d-44f0-8560-3c2d4333ae74/public_url"
  date_start = "2020-10-02"
  date_end = "2023-10-02"
  description = "Juniper Networks Certified Associate"

[[item]]
  organization = "Cisco"
  organization_url = "https://www.cisco.com"
  title = "CCNA Routing and Switching"
  url = ""
  certificate_url = "https://www.youracclaim.com/badges/376cb6bb-05f8-47d3-989c-f91f62fa6a35/public_url"
  date_start = "2019-04-15"
  date_end = "2022-10-15"
  description = "Cisco Certified Network Assocaite Routing and Switching (CCNA Routing and Switching)"
  
[[item]]
  organization = "Cisco"
  organization_url = "https://www.cisco.com"
  title = "CCENT"
  url = ""
  certificate_url = ""
  date_start = "2018-05-30"
  date_end = "2022-10-15"
  description = "Cisco Certified Entry Networking Technician"

+++
