---
# Display name
title: Ryan N Gay

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Electrical Engineer (Networking)

# Organizations/Affiliations
organizations:
- name: ASRC Federal for the Federal Aviation Administration
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: I am a lover of Linux, Apple, and all things that involve networking.

interests:
- Networking
- Security
- Automation

education:
  courses:
  - course: BSc in Computer Science
    institution: Oklahoma Baptist University
    year: 2017

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/ryan-n-gay
- icon: github
  icon_pack: fab
  link: https://github.com/ryan-n-gay
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
---

Growing up, I’ve always been surround by some form of tech. That is how I stumbled into my chosen career path. I’ve always had an inclination to all things tech. When it comes to computers, I guess I am a nerd. So when the opportunity arose to do what I love, I took it. My career started in 2011. I was first introduced to basic networking, and from that moment, I became hooked. Ever since that time, I have had opportunities to work in various areas in IT and Development. Out out all those areas, I discovered that I am a network admin, through and through. I am a lover of Linux, Apple, and all things that involve networking. I always strive to provide the best networking experience possible using my charge.
