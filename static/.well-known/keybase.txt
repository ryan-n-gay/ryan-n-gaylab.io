==================================================================
https://keybase.io/ryanngay
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://blog.ryanngay.com
  * I am ryanngay (https://keybase.io/ryanngay) on keybase.
  * I have a public key ASDGLe7MU-2s5ys_tZ8TlC7rFycplW0EjagCegU2TdeGvQo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120c62deecc53edace72b3fb59f13942eeb172729956d048da8027a05364dd786bd0a",
      "host": "keybase.io",
      "kid": "0120c62deecc53edace72b3fb59f13942eeb172729956d048da8027a05364dd786bd0a",
      "uid": "0a4ab5e69cf32c8f173584c10b4d8f19",
      "username": "ryanngay"
    },
    "merkle_root": {
      "ctime": 1549852283,
      "hash": "de8e96b0ef312b6f477410f5f3304db53fda2a0212d8471366b3c86ece4f861b0c87b21d489d5bd36408e730acf0f22d4a71ced9856b32b67cbe21c001c2bef6",
      "hash_meta": "cae482e191cfe227d9bc4fc1752772c7c79b7bec22ed4bc8b581d6a22fc0e206",
      "seqno": 4739567
    },
    "service": {
      "entropy": "solvvm1jLHANkCjfMxJQXuDV",
      "hostname": "blog.ryanngay.com",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "3.0.0"
  },
  "ctime": 1549852310,
  "expire_in": 504576000,
  "prev": "03978502f7005e362a3ffe437fa44f15e4da5b08baa9e21b411136d7a9b2a126",
  "seqno": 8,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgxi3uzFPtrOcrP7WfE5Qu6xcnKZVtBI2oAnoFNk3Xhr0Kp3BheWxvYWTESpcCCMQgA5eFAvcAXjYqP/5Df6RPFeTaWwi6qeIbQRE216myoSbEINR2qpyPVDIgXC5OEbMadghvMTxvN3b0f5UdERY3fU0CAgHCo3NpZ8RAdaNFGRFcxZvpNY7zhxOo80KYWiIW/71f4fcQHma1La8lrQM8UzXllyDj592Gr21hsOIb/d2SX+eyfTPsCyvGCqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIGGXYpb2D/Q6eCWFi1GGYt9XnjAXZsrjaQbgOzSoehWXo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/ryanngay

==================================================================
